package com.example.quiz7

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_recyclerview_layout.view.*


class CardRecyclerViewAdapter(private val items: List<Array<FieldModel>>) :
    RecyclerView.Adapter<CardRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind() {
            val model = items[adapterPosition]
            itemView.fieldsRecyclerView.layoutManager = LinearLayoutManager(itemView.context)
            val fieldRecyclerViewAdapter = FieldRecyclerViewAdapter(model)
            itemView.fieldsRecyclerView.adapter = fieldRecyclerViewAdapter
        }

    }

}