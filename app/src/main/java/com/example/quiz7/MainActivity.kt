package com.example.quiz7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.input_field_recyclerview_layout.*
import org.json.JSONArray

class MainActivity : AppCompatActivity() {
    private lateinit var cardRecyclerViewAdapter: CardRecyclerViewAdapter
    private var model = listOf<Array<FieldModel>>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        parseJSON()
    }

    private fun init() {
        cardRecyclerViewAdapter = CardRecyclerViewAdapter(model)
        cardRecyclerView.layoutManager = LinearLayoutManager(this)
        cardRecyclerView.adapter = cardRecyclerViewAdapter

        registerButton.setOnClickListener {
            registration()
        }

    }

    private fun parseJSON() {
        val json =
            "[[{\"field_id\":1,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"},{\"field_id\":2,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":3,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}],[{\"field_id\":4,\"hint\":\"Full Name\",\"Field_type\":\"input\",\"keyboard\":\"text\",\"required\":true,\"is_active\":true,\"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"},{\"field_id\":14,\"hint\":\"Jemali\",\"Field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://jemala.png\"},{\"field_id\":89,\"hint\":\"Birthday\",\n" +
                    "\"field_type\":\"chooser\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/S0FnzrQ/birthday.png\"},{\"field_id\":898,\"hint\":\"Gender\",\"field_type\":\"chooser\",\"required\":\"false\",\"is_active\":true,\"icon\":\"https://i.ibb.co/t2C06j7/gender-fluid.png\"},{\"field_id\":1,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"},{\"field_id\":1,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"},{\"field_id\":1,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"},{\"field_id\":1,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"}]]"

        model = Gson().fromJson(json, Array<Array<FieldModel>>::class.java).toList()
        init()

    }

    private fun registration(){
        val requiredFields = arrayListOf<FieldModel>()
        model.forEach { x ->
            x.forEach {
                if(it.required) requiredFields.add(it)
            }

        }
        val emptyFields =  arrayListOf<String>()
        requiredFields.forEach {
            if(fieldEditText.text.isEmpty()){
                emptyFields.add(it.hint)

            }
        }
        infoText.text = "Please Fill Following Fields:"
        val emptyFieldsAdapter = EmptyFieldsAdapter(emptyFields)
        emptyFieldsRecyclerView.layoutManager = LinearLayoutManager(this,  LinearLayoutManager.HORIZONTAL, false)
        emptyFieldsRecyclerView.adapter = emptyFieldsAdapter


    }



}
